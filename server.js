﻿const http = require('http');
const app = require('./app');
const httpServer = http.createServer(app);


// start server
const port = process.env.NODE_ENV === 'production' ? (process.env.PORT || 80) : 8080;
console.log('Worker running process on pids: ' + process.pid+",port: "+port);
httpServer.listen(port);