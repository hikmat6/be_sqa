require('rootpath')();
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const errorHandler = require('_middleware/error-handler');
const routerV1 = require('./api/v1/index');
const fileupload = require('express-fileupload');

/**CORS Avoidance.
 * Asterisk symbol(*) on Access-Control-Allow-Origin
 * should be replace with url for security issue.
 * Only GET, POST, PATCH, DELETE method for now,
 * can add with PUT or others for further.
 */
 app.use((req, res, next) => {
    const corsWhitelist = [
        'http://localhost:3000',
        'http://192.168.2.147:3000',
        'http://192.168.2.123:3000',
        'https://salurqurbanamanah.com',
        'http://192.168.2.16:3000',
    ];
    
    if (corsWhitelist.indexOf(req.headers.origin) !== -1) {
        res.header('Access-Control-Allow-Origin', req.headers.origin);
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    }
    
    if (req.method === 'OPTIONS') {
        res.header(
            'Access-Control-Allow-Methods', 'GET, POST,PUT, PATCH, DELETE'
        );
        return res.status(200).json({});
    }
    next();
});

// global error handler
app.use(errorHandler);

//Body-Parser using for catching body parser (just in case needed)
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//express-fileupload using for file upload
app.use(fileupload({ useTempFiles: true, tempFileDir: './tmp/', createParentPath: false }));

// api routes
app.use('/api/v1', (req, res, next) => {
    next();
}, routerV1);

//module export
module.exports = app;
