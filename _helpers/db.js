const config = require('config.json');
const mysql = require('mysql2/promise');
const { Sequelize } = require('sequelize');

module.exports = db = {};

initialize();

async function initialize() {
    // create db if it doesn't already exist
    const { host, port, user, password, database } = config.database;
    const connection = await mysql.createConnection({ host, port, user, password });
    await connection.query(`CREATE DATABASE IF NOT EXISTS \`${database}\`;`);

    // connect to db
    const sequelize = new Sequelize(database, user, password, { dialect: 'mysql' });

    // init models and add them to the exported db object
    db.Akses = require('../api/models/aksesModel')(sequelize);
    db.Dares = require('../api/models/daresModel')(sequelize);
    db.Donatur = require('../api/models/donaturModel')(sequelize);
    db.Hewan = require('../api/models/hewanModel')(sequelize);
    db.Qurban = require('../api/models/qurbanModel')(sequelize);
    db.Shodaqoh = require('../api/models/shodaqohModel')(sequelize);
    db.statusSalur = require('../api/models/statusSalurModel')(sequelize);
    db.statusTrx = require('../api/models/statusTrxModel')(sequelize);
    db.User = require('../api/models/userModel')(sequelize);

    // sync all models with database
    // await sequelize.sync();
}