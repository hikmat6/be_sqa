const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('../../../_middleware/validate-request');
const authorize = require('../../../_middleware/authorize')
const shodaqohServices = require('../../services/shodaqohService');

// routes
router.get('/', skip, getAll);
router.post('/', skip, insertshodaqoh);
router.put('/:id', skip, update);
router.delete('/:id', skip, _delete);

module.exports = router;

function skip(req, res, next) {
    next();
}

function insertshodaqohSchema(req, res, next) {
    const schema = Joi.object({
        idUser: Joi.string().required(),
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        username: Joi.string().required(),
        password: Joi.string().min(6).required()
    });
    validateRequest(req, next, schema);
}

function getAll(req, res, next) {
    shodaqohServices.getAll()
        .then(shodaqohs => res.json(shodaqohs))
        .catch(next);
}

function insertshodaqoh(req, res, next) {
    console.log('insertshodaqoh')
    shodaqohServices.create(req.body)
        .then(shodaqoh => res.json(shodaqoh))
        .catch(next); 
}

function update(req, res, next) {
    shodaqohServices.update(req.params.id, req.body)
        .then(shodaqoh => res.json(shodaqoh))
        .catch(next);
}

function _delete(req, res, next) {
    shodaqohServices.delete(req.params.id)
        .then(() => res.json({ message: 'shodaqoh deleted successfully' }))
        .catch(next);
}