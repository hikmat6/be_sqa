const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('../../../_middleware/validate-request');
const authorize = require('../../../_middleware/authorize')
const qurbanServices = require('../../services/qurbanService');

// routes
router.get('/', skip, getAll);
router.get('/:id', skip, getbyId);
router.post('/', skip, insertqurban);
router.put('/:id', skip, update);
router.delete('/:id', skip, _delete);

module.exports = router;

function skip(req, res, next) {
    next();
}

function insertqurbanSchema(req, res, next) {
    const schema = Joi.object({
        idUser: Joi.string().required(),
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        username: Joi.string().required(),
        password: Joi.string().min(6).required()
    });
    validateRequest(req, next, schema);
}

function getAll(req, res, next) {
    qurbanServices.getAll()
        .then(qurbans => res.json(qurbans))
        .catch(next);
}

function getbyId(req, res, next) {
    qurbanServices.getById(req.params.id)
        .then(qurbans => res.json(qurbans))
        .catch(next);
}

function insertqurban(req, res, next) {
    console.log('insertqurban')
    qurbanServices.create(req.body)
        .then(() => res.json("insert data qurban berhasil"))
        .catch(next); 
}

function update(req, res, next) {
    qurbanServices.update(req.params.id, req.body)
        .then(qurban => res.json(qurban))
        .catch(next);
}

function _delete(req, res, next) {
    qurbanServices.delete(req.params.id)
        .then(() => res.json({ message: 'qurban deleted successfully' }))
        .catch(next);
}