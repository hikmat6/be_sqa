const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('../../../_middleware/validate-request');
const authorize = require('../../../_middleware/authorize')
const statusSalurServices = require('../../services/stsSalurService');

// routes
router.get('/', skip, getAll);
router.post('/', skip, insertstatusSalur);
router.put('/:id', skip, update);
router.delete('/:id', skip, _delete);

module.exports = router;

function skip(req, res, next) {
    next();
}

function insertstatusSalurSchema(req, res, next) {
    const schema = Joi.object({
        idUser: Joi.string().required(),
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        username: Joi.string().required(),
        password: Joi.string().min(6).required()
    });
    validateRequest(req, next, schema);
}

function getAll(req, res, next) {
    statusSalurServices.getAll()
        .then(statusSalurs => res.json(statusSalurs))
        .catch(next);
}

function insertstatusSalur(req, res, next) {
    console.log('insertstatusSalur')
    statusSalurServices.create(req.body)
        .then(statusSalur => res.json(statusSalur))
        .catch(next);
}

function update(req, res, next) {
    statusSalurServices.update(req.params.id, req.body)
        .then(statusSalur => res.json(statusSalur))
        .catch(next);
}

function _delete(req, res, next) {
    statusSalurServices.delete(req.params.id)
        .then(() => res.json({ message: 'statusSalur deleted successfully' }))
        .catch(next);
}