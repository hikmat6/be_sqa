//Lib
const express = require('express');
const router = express.Router();
const routerAkses = require('../v1/akses/aksesController');
const routerDares = require('../v1/dares/daresController');
const routerDonatur = require('../v1/donatur/donaturController');
const routerHewan = require('../v1/hewan/hewanController');
const routerQurban = require('../v1/qurban/qurbanController');
const routerShodaqoh = require('../v1/shodaqoh/shodaqohController');
const routerStatusSalur = require('../v1/statusSalur/statusSalurController');
const routerStatusTrx = require('../v1/statusTrx/statusTrxController');
const routerUser = require('../v1/users/usersController');

router.use('/akses', routerAkses);
router.use('/dares', routerDares);
router.use('/donatur', routerDonatur);
router.use('/hewan', routerHewan);
router.use('/qurban', routerQurban);
router.use('/shodaqoh', routerShodaqoh);
router.use('/statussalur', routerStatusSalur);
router.use('/statustrx', routerStatusTrx);
router.use('/users', routerUser);
module.exports = router;