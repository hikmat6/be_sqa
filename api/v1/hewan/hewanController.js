const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('../../../_middleware/validate-request');
const authorize = require('../../../_middleware/authorize')
const hewanServices = require('../../services/hewanService');
const fs = require('fs');
const path = require('path');

// routes
router.get('/', skip, getAll);
router.get('/:id', skip, getbyId);
router.post('/', skip, insertHewan);
router.put('/:id', skip, update);
router.delete('/:id', skip, _delete);

module.exports = router;

function skip(req, res, next) {
    next();
}

function insertHewanSchema(req, res, next) {
    const schema = Joi.object({
        idUser: Joi.string().required(),
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        username: Joi.string().required(),
        password: Joi.string().min(6).required()
    });
    validateRequest(req, next, schema);
}

function getAll(req, res, next) {
    hewanServices.getAll()
        .then(hewans => res.json(hewans))
        .catch(next);
}

function getbyId(req, res, next) {
    hewanServices.getById(req.params.id)
        .then(hewans => res.json(hewans))
        .catch(next);
}

function insertHewan(req, res, next) {
    console.log('insertHewan')
    console.log(req.body)
    let targetFileVideos = "";
    let extNameVideos = "";
    let baseNameVideos = "";
    let uploadDirVideos = "";
    let numVideos = "";
    let targetFileImages = "";
    let extNameImg = "";
    let baseNameImg = "";
    let uploadDirImg = "";
    let imgList = "";
    let numImg = "";
    // console.log(req.files.foto)
    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).send('No files were uploaded.');
    }else{
        if(req.files.video){
            targetFileVideos = req.files.video;
            extNameVideos =  path.extname(targetFileVideos.name);
            baseNameVideos = path.basename(targetFileVideos.name,extNameVideos);
            uploadDirVideos = path.join(__dirname, '../../../public/videos', targetFileVideos.name);
            console.log(extNameVideos)

            if(targetFileVideos.size > 10148576){
                fs.unlinkSync(targetFileVideos.tempFilePath);
                return res.status(413).send("File is too Large");
            }
            
            numVideos = 1;
            while(fs.existsSync(uploadDirVideos)){
                uploadDirVideos = path.join(__dirname, '../../../public/videos', baseNameVideos + '-' + num + extNameVideos);
                numVideos++;
            }

            targetFileVideos.mv(uploadDirVideos, (err) => {
                if (err)
                    return res.status(500).send(err);        
                
                req.body.video = baseNameVideos + ''+ extNameVideos;                
            });
        }
        console.log(req.files.foto)
        targetFileImages = req.files.foto;
        extNameImg = path.extname(targetFileImages.name);
        baseNameImg = path.basename(targetFileImages.name, extNameImg);
        uploadDirImg = path.join(__dirname, '../../../public/images', targetFileImages.name);

        imgList = ['.png','.jpg','.jpeg','.gif'];
        // Checking the file type
        if(!imgList.includes(extNameImg)){
            fs.unlinkSync(targetFileImages.tempFilePath);
            return res.status(422).send("Invalid Image");
        }

        if(targetFileImages.size > 1048576){
            fs.unlinkSync(targetFileImages.tempFilePath);
            return res.status(413).send("File is too Large");
        }

        numImg = 1;
        while(fs.existsSync(uploadDirImg)){
            uploadDirImg = path.join(__dirname, '../../../public/images', baseNameImg + '-' + num + extNameImg);
            numImg++;
        }
        
        targetFileImages.mv(uploadDirImg, (err) => {
            if (err)
                return res.status(500).send(err);        
            
            req.body.foto = baseNameImg + ''+ extNameImg;
            hewanServices.create(req.body)
                .then(() => res.json({ message: 'Input Data Hewan successful' }))
                .catch( err => {
                    console.log(err)
                });            
        });
    }
  
}

function update(req, res, next) {
    hewanServices.update(req.params.id, req.body)
        .then(hewan => res.json(hewan))
        .catch(next);
}

function _delete(req, res, next) {
    hewanServices.delete(req.params.id)
        .then(() => res.json({ message: 'Hewan deleted successfully' }))
        .catch(next);
}