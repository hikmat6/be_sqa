const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('../../../_middleware/validate-request');
const authorize = require('../../../_middleware/authorize')
const statusTrxServices = require('../../services/stsTrxService');

// routes
router.get('/', skip, getAll);
router.post('/', skip, insertstatusTrx);
router.put('/:id', skip, update);
router.delete('/:id', skip, _delete);

module.exports = router;

function skip(req, res, next) {
    next();
}

function insertstatusTrxSchema(req, res, next) {
    const schema = Joi.object({
        idUser: Joi.string().required(),
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        username: Joi.string().required(),
        password: Joi.string().min(6).required()
    });
    validateRequest(req, next, schema);
}

function getAll(req, res, next) {
    statusTrxServices.getAll()
        .then(statusTrxs => res.json(statusTrxs))
        .catch(next);
}

function insertstatusTrx(req, res, next) {
    console.log('insertstatusTrx')
    statusTrxServices.create(req.body)
        .then(statusTrx => res.json(statusTrx))
        .catch(next);
}

function update(req, res, next) {
    statusTrxServices.update(req.params.id, req.body)
        .then(statusTrx => res.json(statusTrx))
        .catch(next);
}

function _delete(req, res, next) {
    statusTrxServices.delete(req.params.id)
        .then(() => res.json({ message: 'statusTrx deleted successfully' }))
        .catch(next);
}