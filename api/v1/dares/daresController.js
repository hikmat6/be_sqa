const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('../../../_middleware/validate-request');
const authorize = require('../../../_middleware/authorize')
const daresServices = require('../../services/daresService');

// routes
router.get('/', skip, getAll);
router.get('/:id', skip, getById);
router.post('/', skip, insertdares);
router.put('/:id', skip, update);
router.delete('/:id', skip, _delete);

module.exports = router;

function skip(req, res, next) {
    next();
}

function insertdaresSchema(req, res, next) {
    const schema = Joi.object({
        idUser: Joi.string().required(),
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        username: Joi.string().required(),
        password: Joi.string().min(6).required()
    });
    validateRequest(req, next, schema);
}

function getAll(req, res, next) {
    daresServices.getAll()
        .then(daress => res.json(daress))
        .catch(next);
}

function getById(req, res, next) {
    daresServices.getById(req.params.id)
        .then(daress => res.json(daress))
        .catch(next);
}

function insertdares(req, res, next) {
    daresServices.create(req.body)
        .then(() => res.json({ message: 'Input Data Dares successful' }))
        .catch(next);
}

function update(req, res, next) {
    daresServices.update(req.params.id, req.body)
        .then(dares => res.json(dares))
        .catch(next);
}

function _delete(req, res, next) {
    daresServices.delete(req.params.id)
        .then(() => res.json({ message: 'dares deleted successfully' }))
        .catch(next);
}