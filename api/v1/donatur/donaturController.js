const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('../../../_middleware/validate-request');
const authorize = require('../../../_middleware/authorize')
const donaturServices = require('../../services/donaturService');

// routes
router.get('/', skip, getAll);
router.get('/:id', skip, getbyId);
router.post('/', skip, insertdonatur);
router.put('/:id', skip, update);
router.delete('/:id', skip, _delete);

module.exports = router;

function skip(req, res, next) {
    next();
}

function insertdonaturSchema(req, res, next) {
    const schema = Joi.object({
        idUser: Joi.string().required(),
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        username: Joi.string().required(),
        password: Joi.string().min(6).required()
    });
    validateRequest(req, next, schema);
}

function getAll(req, res, next) {
    donaturServices.getAll()
        .then(donaturs => res.json(donaturs))
        .catch(next);
}

function getbyId(req, res, next) {
    donaturServices.getById(req.params.id)
        .then(donaturs => res.json(donaturs))
        .catch(next);
}

function insertdonatur(req, res, next) {
    console.log('insertdonatur')
    donaturServices.create(req.body)
        .then(() => res.json({ message: 'Input Data Donatur successful' }))
        .catch(next);  
}

function update(req, res, next) {
    donaturServices.update(req.params.id, req.body)
        .then(donatur => res.json(donatur))
        .catch(next);
}

function _delete(req, res, next) {
    donaturServices.delete(req.params.id)
        .then(() => res.json({ message: 'donatur deleted successfully' }))
        .catch(next);
}