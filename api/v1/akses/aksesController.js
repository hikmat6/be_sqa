const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('../../../_middleware/validate-request');
const authorize = require('../../../_middleware/authorize')
const aksesServices = require('../../services/aksesService');

// routes
router.get('/', skip, getAll);
router.get('/:id', skip, getById);
router.post('/', skip, insertakses);
router.put('/:id', skip, update);
router.delete('/:id', skip, _delete);

module.exports = router;

function skip(req, res, next) {
    next();
}

function insertaksesSchema(req, res, next) {
    const schema = Joi.object({
        idUser: Joi.string().required(),
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        username: Joi.string().required(),
        password: Joi.string().min(6).required()
    });
    validateRequest(req, next, schema);
}

function getAll(req, res, next) {
    aksesServices.getAll()
        .then(aksess => res.json(aksess))
        .catch(next);
}

function getById(req, res, next) {
    aksesServices.getById(req.params.id)
        .then(akses => res.json(akses))
        .catch(next);
}

function insertakses(req, res, next) {
    aksesServices.create(req.body)
        .then(() => res.json({ message: 'Input Data Akses successful' }))
        .catch(next);
}

function update(req, res, next) {
    aksesServices.update(req.params.id, req.body)
        .then(akses => res.json(akses))
        .catch(next);
}

function _delete(req, res, next) {
    aksesServices.delete(req.params.id)
        .then(() => res.json({ message: 'Akses deleted successfully' }))
        .catch(next);
}