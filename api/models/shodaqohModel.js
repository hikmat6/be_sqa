const { DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        id_kupon : {type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true},
        kode_kupon: { type: DataTypes.STRING, allowNull: false },
        id_donatur: { type: DataTypes.INTEGER, allowNull: false },
        nominal: { type: DataTypes.INTEGER, allowNull: false },        
        metode_bayar: { type: DataTypes.STRING, allowNull: false },
        tgl_bayar: { type: DataTypes.DATE, allowNull: true },
        id_statusTrx: { type: DataTypes.INTEGER, allowNull: false }
    };

    const options = {
        defaultScope: {
            // exclude hash by default
            attributes: { exclude: ['hash'] }
        },
        scopes: {
            // include hash with this scope
            withHash: { attributes: {}, }
        },
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: false,

        // If don't want createdAt
        createdAt: false,

        // If don't want updatedAt
        updatedAt: false,
    };

    return sequelize.define('Shodaqoh', attributes, options);
}