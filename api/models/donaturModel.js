const { DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {    
    const attributes = {
        id_donatur : {type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true},
        nama_donatur: { type: DataTypes.STRING, allowNull: false },
        email: { type: DataTypes.STRING, allowNull: true },
        telepon: { type: DataTypes.STRING, allowNull: false },
        alamat: { type: DataTypes.STRING, allowNull: false },
        atas_nama: { type: DataTypes.STRING, allowNull: true },
    };

    const options = {
        defaultScope: {
            // exclude hash by default
            attributes: { exclude: ['hash'] }
        },
        scopes: {
            // include hash with this scope
            withHash: { attributes: {}, }
        },
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: false,

        // If don't want createdAt
        createdAt: false,

        // If don't want updatedAt
        updatedAt: false,
    };

    return sequelize.define('Donatur', attributes, options);
}