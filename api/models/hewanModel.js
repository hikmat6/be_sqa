const { DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        id_hewan : {type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true},
        kode_hewan: { type: DataTypes.STRING, allowNull: false },
        jenis_hewan: { type: DataTypes.STRING, allowNull: false },
        harga_hewan: { type: DataTypes.INTEGER, allowNull: false },
        foto: { type: DataTypes.STRING, allowNull: false },
        video: { type: DataTypes.STRING, allowNull: false },
        bobot: { type: DataTypes.STRING, allowNull: false },
        stok: { type: DataTypes.INTEGER, allowNull: false }
    };

    const options = {
        defaultScope: {
            // exclude hash by default
            attributes: { exclude: ['hash'] }
        },
        scopes: {
            // include hash with this scope
            withHash: { attributes: {}, }
        },
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: false,

        // If don't want createdAt
        createdAt: false,

        // If don't want updatedAt
        updatedAt: false,
    };

    return sequelize.define('Hewan', attributes, options);
}