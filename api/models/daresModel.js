const { DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {    
    const attributes = {
        id_dares : {type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true},
        nama_dares: { type: DataTypes.STRING, allowNull: false },
        alamat: { type: DataTypes.STRING, allowNull: false },
        wilayah: { type: DataTypes.STRING, allowNull: false }
    };

    const options = {
        defaultScope: {
            // exclude hash by default
            attributes: { exclude: ['hash'] }
        },
        scopes: {
            // include hash with this scope
            withHash: { attributes: {}, }
        },
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: false,

        // If don't want createdAt
        createdAt: false,

        // If don't want updatedAt
        updatedAt: false,
    };

    return sequelize.define('Dares', attributes, options);
}