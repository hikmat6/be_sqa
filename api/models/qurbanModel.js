const { DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        id_faktur : {type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true},
        kode_faktur: { type: DataTypes.STRING, allowNull: false },
        id_donatur: { type: DataTypes.INTEGER, allowNull: false },
        id_hewan: { type: DataTypes.INTEGER, allowNull: false },
        id_helper: { type: DataTypes.INTEGER, allowNull: true },
        id_statusSalur: { type: DataTypes.INTEGER, allowNull: true },
        id_dares: { type: DataTypes.INTEGER, allowNull: true },
        alamat_kirim: { type: DataTypes.STRING, allowNull: true },
        minta_bagian: { type: DataTypes.STRING, allowNull: true },
        metode_bayar: { type: DataTypes.STRING, allowNull: false },
        tgl_bayar: { type: DataTypes.DATE, allowNull: true },
        id_statusTrx: { type: DataTypes.INTEGER, allowNull: false },
        update_by: { type: DataTypes.INTEGER, allowNull: true }
    };

    const options = {
        defaultScope: {
            // exclude hash by default
            attributes: { exclude: ['hash'] }
        },
        scopes: {
            // include hash with this scope
            withHash: { attributes: {}, }
        },
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: false,

        // If don't want createdAt
        createdAt: false,

        // If don't want updatedAt
        updatedAt: false,
    };

    return sequelize.define('Qurban', attributes, options);
}