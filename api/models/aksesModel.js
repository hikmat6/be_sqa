const { DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {    
    const attributes = {
        id_hak_akses : {type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true},
        nama_hak_akses: { type: DataTypes.STRING, allowNull: false }
    };

    const options = {
        defaultScope: {
            // exclude hash by default
            attributes: { exclude: ['hash'] }
        },
        scopes: {
            // include hash with this scope
            withHash: { attributes: {}, }
        },
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: false,

        // If don't want createdAt
        createdAt: false,

        // If don't want updatedAt
        updatedAt: false,
    };

    return sequelize.define('Akses_manajemen', attributes, options);
}