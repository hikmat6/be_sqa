const db = require('../../_helpers/db');
const Joi = require('joi');

module.exports = {
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await db.Shodaqoh.findAll();
}

async function getById(id) {
    return await getShodaqoh(id);
}

async function create(params) {
    // save shodaqoh
    console.log(params)
    let Donatur = {
        'nama_donatur' : params.nama_donatur,
        'telepon': params.telepon,
        'alamat': params.alamat
    }
    console.log(Donatur)
    const d = new Date.now();
    const donatur_awal= await db.Donatur.create(Donatur);
    const angkaRandom = Math.floor((Math.random() * 5000) + 1);
    const kupon = 'SQA' + '-' + angkaRandom;
    let Shodaqoh = {
        'kode_kupon' : kupon,
        'id_donatur': donatur_awal.id_donatur,
        'nominal': params.nominal,
        'metode_bayar': params.metode_bayar,
        'id_statusTrx': 1,
        'tgl_bayar': d.getDate()
    }
    console.log(Shodaqoh)
    const feedback = await db.Shodaqoh.create(Shodaqoh);
    return feedback;
}

async function update(id, params) {
    const shodaqoh = await getShodaqoh(id);

    // copy params to shodaqoh and save
    Object.assign(shodaqoh, params);
    await shodaqoh.save();

}

async function _delete(id) {
    const shodaqoh = await getShodaqoh(id);
    await shodaqoh.destroy();
}

// helper functions

async function getShodaqoh(id) {
    const shodaqoh = await db.Shodaqoh.findOne({where: { id_shodaqoh: id }})
    if (!shodaqoh) throw 'shodaqoh not found';
    return shodaqoh;
}

