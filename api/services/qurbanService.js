const db = require('../../_helpers/db');

module.exports = {
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await db.Qurban.findAll();
}

async function getById(id) {
    return await getQurban(id);
}

async function create(params) {
    // save qurban
    await db.Qurban.create(params);
}

async function update(id, params) {
    const qurban = await getQurban(id);

    // copy params to qurban and save
    Object.assign(qurban, params);
    await qurban.save();
    return qurban.get();

}

async function _delete(id) {
    const qurban = await getQurban(id);
    await qurban.destroy();
}

// helper functions

async function getQurban(id) {
    const qurban = await db.Qurban.findOne({where: { id_faktur: id }})
    if (!qurban) throw 'qurban not found';
    return qurban;
}

