const db = require('../../_helpers/db');

module.exports = {
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await db.Donatur.findAll();
}

async function getById(id) {
    return await getDonatur(id);
}

async function create(params) {
    // save donatur
    await db.Donatur.create(params);
}

async function update(id, params) {
    const donatur = await getDonatur(id);

    // copy params to donatur and save
    Object.assign(donatur, params);
    await donatur.save();
    return donatur.get();

}

async function _delete(id) {
    const donatur = await getDonatur(id);
    await donatur.destroy();
}

// helper functions

async function getDonatur(id) {
    const donatur = await db.Donatur.findOne({where: { id_donatur: id }})
    if (!donatur) throw 'donatur not found';
    return donatur;
}

