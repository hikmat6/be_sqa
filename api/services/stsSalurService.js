const db = require('../../_helpers/db');

module.exports = {
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await db.statusSalur.findAll();
}

async function getById(id) {
    return await getstatusSalur(id);
}

async function create(params) {
    // save statusSalur
    await db.statusSalur.create(params);
}

async function update(id, params) {
    const statusSalur = await getstatusSalur(id);

    // copy params to statusSalur and save
    Object.assign(statusSalur, params);
    await statusSalur.save();

}

async function _delete(id) {
    const statusSalur = await getstatusSalur(id);
    await statusSalur.destroy();
}

// helper functions

async function getstatusSalur(id) {
    const statusSalur = await db.statusSalur.findOne({where: { id_statusSalur: id }})
    if (!statusSalur) throw 'statusSalur not found';
    return statusSalur;
}

