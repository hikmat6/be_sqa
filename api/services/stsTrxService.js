const db = require('../../_helpers/db');

module.exports = {
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await db.statusTrx.findAll();
}

async function getById(id) {
    return await getstatusTrx(id);
}

async function create(params) {
    // save statusTrx
    await db.statusTrx.create(params);
}

async function update(id, params) {
    const statusTrx = await getstatusTrx(id);

    // copy params to statusTrx and save
    Object.assign(statusTrx, params);
    await statusTrx.save();

}

async function _delete(id) {
    const statusTrx = await getstatusTrx(id);
    await statusTrx.destroy();
}

// helper functions

async function getstatusTrx(id) {
    const statusTrx = await db.statusTrx.findOne({where: { id_statusTrx: id }})
    if (!statusTrx) throw 'statusTrx not found';
    return statusTrx;
}

