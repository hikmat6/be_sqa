const db = require('../../_helpers/db');

module.exports = {
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await db.Dares.findAll();
}

async function getById(id) {
    return await getDares(id);
}

async function create(params) {
    // save dares
    await db.Dares.create(params);
}

async function update(id, params) {
    const dares = await getDares(id);

    // copy params to dares and save
    Object.assign(dares, params);
    await dares.save();
    return dares.get();

}

async function _delete(id) {
    const dares = await getDares(id);
    await dares.destroy();
}

// helper functions

async function getDares(id) {
    const dares = await db.Dares.findOne({where: { id_dares: id }})
    if (!dares) throw 'dares not found';
    return dares;
}

