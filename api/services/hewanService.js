const db = require('../../_helpers/db');

module.exports = {
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await db.Hewan.findAll();
}

async function getById(id) {
    return await getHewan(id);
}

async function create(params) {
    // save hewan
    await db.Hewan.create(params);
}

async function update(id, params) {
    const hewan = await getHewan(id);

    // copy params to hewan and save
    Object.assign(hewan, params);
    await hewan.save();
    return hewan.get();

}

async function _delete(id) {
    const hewan = await getHewan(id);
    await hewan.destroy();
    return hewan.get();
}

// helper functions

async function getHewan(id) {
    const hewan = await db.Hewan.findOne({where: { id_hewan: id }})
    if (!hewan) throw 'Hewan not found';
    return hewan;
}

