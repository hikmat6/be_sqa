const db = require('../../_helpers/db');

module.exports = {
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await db.Akses.findAll();
}

async function getById(id) {
    return await getAkses(id);
}

async function create(params) {
    // save akses
    await db.Akses.create(params);
}

async function update(id, params) {
    const akses = await getAkses(id);
    // copy params to akses and save
    Object.assign(akses, params);
    await akses.save();
    return akses.get();
}

async function _delete(id) {
    const akses = await getAkses(id);
    await akses.destroy();
}

// helper functions
async function getAkses(id) {
    const akses = await db.Akses.findOne({where: { id_hak_akses: id }})
    if (!akses) throw 'akses not found';
    return akses;
}

